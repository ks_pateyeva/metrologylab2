program LR2;

uses
  Vcl.Forms,
  uForm in 'uForm.pas' {MainForm},
  uPerlOperators in 'uPerlOperators.pas',
  uStack in 'uStack.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
