#!/usr/local/bin/perl

use warnings;
use strict;
use Getopt::Long;
use POSIX;

my $filename = $ARGV[2];

my $e = 0;
my $d = 0;
my $vernam = 0;
my $vigenere = 0;
my $caesar = 0;

GetOptions(
  'e' => \$e,
  'd' => \$d,
  'vernam' => \$vernam,
  'vigenere' => \$vigenere,
  'caesar' => \$caesar
  );

if ($e) {
  if ($vernam) {
    encrypt_vernam();
  } 
  elsif ($vigenere) {
    encrypt_vigenere();
  }
  elsif ($caesar) {
    encrypt_caesar();
  }
}

elsif ($d) {
  if ($vernam) {
    decrypt_vernam();
  } 
  elsif ($vigenere) {
    decrypt_vigenere();
  }
  elsif ($caesar) {
    decrypt_caesar();
  }
}

sub encrypt_vernam {
  open my $encrypted, '>', 'encrypted.txt';
  my $shift_sequence = ""; # initialize empty string

  while ($string =~ /(.)/g) {
    my $range = rand(length($string));
    my $shift = floor(rand($range) + 1);

    $shift_sequence = $shift_sequence . $shift . " ";
    if ((ord($1) + $shift) > 128) {
      print $encrypted chr(ord($1) + $shift - 128);
    }
    else {
      print $encrypted chr(ord($1) + $shift);
    }
  }

  print "\n";
  print "$file successfully encrypted using Vernam cipher.\nencrypted.txt created in current directory.\nOne-time pad: $shift_sequence\n";
}

sub decrypt_vernam {
  open my $decrypted, '>', 'decrypted.txt';

  print "Please enter a shift sequence, separated by spaces (e.g. 5 1 4 6 4 3 3 1 2): ";
  my $shift_sequence = <STDIN>; 

  my $file = $filename;
  my @shift_array = split / /, $shift_sequence;
  
  print "\n"; 

  my $i = 0;
  while ($string =~ /(.)/g) {
    my $shift = $shift_array[$i];
 
    if ((ord($1) - $shift) < 0) {
      print $decrypted chr(ord($1) - $shift + 128);
    }
    else {
      print $decrypted chr(ord($1) - $shift);
    }
    $i = $i + 1;
  }

  print "$file successfully decrypted using Vernam cipher.\ndecrypted.txt created in current directory.\n";
}

sub encrypt_vigenere {
  open my $encrypted, '>', 'encrypted.txt';

  print "Please enter a keyword: ";
  my $keyword = <STDIN>; 

  my @characters = split //, $keyword;
  my $file = $filename;
  my $string = do {
    local $/ = undef;
    open my $fh, "<", $file
    or die "could not open $file: $!";
    <$fh>;
  };
  my $length = scalar @characters;
  my $range = 128;
  

  print "\n";
  print "Keyword: $keyword";

  my $i = 0;
  while ($string =~ /./g) {
    
    my $shift = ord($characters[$i]);
    if ((ord($1) + $shift) > 128) {
      print $encrypted chr(ord($1) + $shift - 128);
    }
    else {
      print $encrypted chr(ord($1) + $shift);
    }
   
    if ($i == ($length - 1)) {
      $i = 0;
    }
    else {
      $i = $i + 1;
    }
  }

  print "$file successfully encrypted using Vigenere cipher.\nencrypted.txt created in current directory.\nKeyword: $keyword\n";
}

sub decrypt_vigenere {
  open my $decrypted, '>', 'decrypted.txt';
  print "Please enter a keyword: ";
  my $keyword = <STDIN>; 

  my @characters = split //, $keyword;

  my $file = $filename;

  my $length = scalar @characters;
  my $range = 128;

  print "Keyword: $keyword";

  print "\n";

  my $i = 0;
  while ($string =~ /(.)/g) {
    floor(rand($range) + 1);
    
    my $shift = ord($characters[$i]);
    if ((ord($1) - $shift) < 0) {
      print $decrypted chr(ord($1) - $shift + 128);
    }
    else {
      print $decrypted chr(ord($1) - $shift);
    }
  
    if ($i == ($length - 1)) {
      $i = 0;
    }
    else {
      $i = $i + 1;
    }
  }

  print "$file successfully decrypted using Vigenere cipher.\ndecrypted.txt created in current directory.\n";
}

sub encrypt_caesar {
  open my $encrypted, '>', 'encrypted.txt';

  my $range = 128; # size of ascii character set
  my $shift = floor(rand($range) + 1);

  print "\n";
 print "Shift: $shift\n";
 my $file = $filename;
  
  while ($string =~ /./g) {
    if ((ord($1) + $shift) > 128) {
      print $encrypted chr(ord($1) + $shift - 128);
    }
    else {
      print $encrypted chr(ord($1) + $shift);
    }
  }
}

sub decrypt_caesar {
  open my $decrypted, '>', 'decrypted.txt';

  print "Please enter a shift (e.g. 5): ";
  my $shift = <STDIN>; 

  my $range = 128; # size of ascii character set
  print "Shift: $shift\n";

  my $file = $filename;

  print "\n";
  
  while ($string =~ /(.)/g) {
    if ((ord($1) - $shift) < 0) {
      print $decrypted chr(ord($1) - $shift + 128);
    }
    else {
      print $decrypted chr(ord($1) - $shift); 
    }
  }
  print "$file successfully decrypted using Caesar cipher.\ndecrypted.txt created in current directory.\n";
}

sub metrics() {
	my $a = 5;
	if ($a == 8) {
		for (my $i = 0; $i < 8; $i++) {
			for (my $j = 0; $j < 8; $j++) {
				print "a = 8";
			}
			print "\n";
		} 
	}
	elsif ($a == 7) {
		print "a = 7";
	}
	else {
		for (my $i = 0; $i < 5; $i++) {
			for (my $j = 0; $j < 5; $j++) {
				for (my $k = 0; $k < 5; $k++) {
					for (my $l = 0; $l < 5; $l++) {
						for (my $m = 0; $m < 5; $m++) {
							if ($l == $m) {
								print "l = m";
							}
						}
					}
				}
			}
		}
	}
}