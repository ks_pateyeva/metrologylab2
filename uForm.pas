﻿unit uForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Math,
  Vcl.Buttons;

type
  TMainForm = class(TForm)
    memoText: TMemo;
    lbMetrics: TLabel;
    strgrJilbMetrics: TStringGrid;
    bbtnClose: TBitBtn;
    bbtnRetry: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure CountMetrics;
    procedure bbtnRetryClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses
   uPerlOperators;

procedure TMainForm.bbtnRetryClick(Sender: TObject);
begin
   CountMetrics;
end;

procedure TMainForm.CountMetrics;
var
   programLine, programText: string;
   i, condition: integer;
begin
   memoText.Lines.LoadFromFile('PerlProgram.pl');
   programText := '';
   for i := 0 to memoText.Lines.Count - 1 do
   begin
      programLine := memoText.Lines[i];
      DeleteComments(programLine);
      programText := concat(programText, programLine);
   end;
   condition := 0;
   for i := 0 to High(CONDITIONS) do
      condition := condition + FindOperator(programText, CONDITIONS[i]);
   strgrJilbMetrics.Cells[1, 1] := FloatToStrf(condition / AllOperators(programText), ffGeneral, 3, 2);
   strgrJilbMetrics.Cells[1, 0] := IntToStr(condition);
   programLine := copy(programText, pos('metrics', programText) , Length(programText) - pos('metrics', programText));
   strgrJilbMetrics.Cells[1, 2] := IntToStr(NestingLevel(programLine) - 1);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
   strgrJilbMetrics.Cells[0, 0] := 'Абсолютная сложность';
   strgrJilbMetrics.Cells[0, 1] := 'Относительная сложность';
   strgrJilbMetrics.Cells[0, 2] := 'Макс. уровень вложенности';
   CountMetrics();
end;

end.
