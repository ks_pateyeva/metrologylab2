unit uStack;

interface

type
   TElementType = (str, int);
   TStackPointer = ^TStack;
   TStack = record
      prev: TStackPointer;
      value: integer;
   end;

function pop(var stack: TStackPointer): integer;
procedure push(var stack: TStackPointer; value: integer);

implementation

function pop(var stack: TStackPointer): integer;
begin
   if stack <> nil then
   begin
      result := stack^.value;
      stack := stack^.prev
   end
   else
      result := 0
end;

procedure push(var stack: TStackPointer; value: integer);
var
   t: TStackPointer;
begin
   new(t);
   t^.prev := stack;
   t^.value := value;
   stack := t;
end;

end.
