unit uPerlOperators;

interface

const
   //�������� ���������
   IF_OPERATOR = 'if';
//	IF_ELSE_OPERATOR = 'if-else';
   ELSIF_OPERATOR = 'elsif';
//	TERNARY_OPERATOR = ':?';

   //��������� ���������
   ASSIGNMENT = '=';      // ���������� - ��������?
   FOR_OPERATOR = 'for';
   UNTIL_OPERATOR = 'until';
   WHILE_OPERATOR = 'while';
   FOREACH_OPERATOR = 'foreach';
   NEXT_OPERATOR = 'next';
   REDO_OPERATOR = 'redo';
   LAST_OPERATOR = 'last';

   OPERATORS: array [0..9] of shortstring = (IF_OPERATOR, ASSIGNMENT,
                                             ELSIF_OPERATOR, FOR_OPERATOR,
                                             UNTIL_OPERATOR, FOREACH_OPERATOR,
                                             NEXT_OPERATOR, REDO_OPERATOR,
                                             LAST_OPERATOR, WHILE_OPERATOR);
   CONDITIONS: array [0..4] of shortstring = (IF_OPERATOR, WHILE_OPERATOR,
                                          UNTIL_OPERATOR, FOREACH_OPERATOR,
                                          FOR_OPERATOR);

procedure DeleteComments(var programLine: string);
procedure DeleteLiterals(var programLine: string);
function AllOperators(programLine: string): integer;
function FindOperator(const programLine, PerlOperator: string) : integer;
function NestingLevel(const programLine: string): integer;

implementation

uses
   uStack, System.Math;

function minOf6(a, b, c, d, e, f: integer): integer;
var
   temp: array [0..5] of integer;
   i, j: integer;
begin
   i := 0;
   temp[0] := 0;
   if a <> 0 then
   begin
      temp[i] := a;
      inc(i)
   end;
   if b <> 0 then
   begin
      temp[i] := b;
      inc(i)
   end;
   if c <> 0 then
   begin
      temp[i] := c;
      inc(i)
   end;
   if d <> 0 then
   begin
      temp[i] := d;
      inc(i)
   end;
   if e <> 0 then
   begin
      temp[i] := e;
      inc(i)
   end;
   if f <> 0 then
   begin
      temp[i] := f;
      inc(i)
   end;
   result := temp[0];
   for j := 1 to i - 1 do
      if temp [j] < result then
         result := temp[j]
end;

function NestingLevel(const programLine: string): integer;
var
   stack: TStackPointer;
   i, position1, j, position2, currNesting: integer;
begin
   result := 0;
   currNesting := 0;
   i := 1;
   stack := nil;
   while (i < Length(programLine)) do
   begin
      position1 := minOf6(pos('if', programLine, i), pos('until', programLine, i),
                          pos('while', programLine, i), pos('for', programLine, i),
                          pos('foreach', programLine, i), pos('else', programLine, i));
      if position1 <> 0 then
      begin
         push(stack, position1);
         inc(currNesting);
         result := max(result, currNesting);
         i := pos('{', programLine, position1);
         position1 := minOf6(pos('if', programLine, i), pos('until', programLine, i),
                          pos('while', programLine, i), pos('for', programLine, i),
                          pos('foreach', programLine, i), pos('else', programLine, i));
         position2 := pos('}', programLine, i);
          if (position1 = 0) or (position2 = 0) then
            break;
         while position1 > position2 do
         begin
            pop(stack);
            dec(currNesting);
            position2 := pos('}', programLine, position2 + 1);
         end;
         i := position1;
      end
      else
         break;
   end;
end;

procedure DeleteComments(var programLine: string);
const
   COMMENT_SYMBOL = '#';
var
   i: integer;
begin
   for i := 1 to Length(programLine) do
      if (Length(programLine) > 0) and (programLine[i] =  COMMENT_SYMBOL) then
         delete(programLine, i, Length(programLine) - i + 1);
end;

procedure DeleteLiterals(var programLine: string);
const
   STRING_SYMBOL_1 = '"';
   STRING_SYMBOL_2 = '''';
var
   i: integer;
begin
   i := 1;
   while i < Length(programLine) + 1 do
      if programLine[i] = STRING_SYMBOL_1 then
         delete(programLine, i, pos(STRING_SYMBOL_1, programLine, i + 1) - i + 1)
      else
         inc(i);
   i := 1;
   while i < Length(programLine) + 1 do
      if programLine[i] = STRING_SYMBOL_2 then
         delete(programLine, i, pos(STRING_SYMBOL_2, programLine, i + 1) - i + 1)
      else
         inc(i);
end;

function AllOperators(programLine: string): integer;
var
   i, position: integer;
begin
   result := 0;
   for i := 0 to High(OPERATORS) do
      while true do
      begin
         position := pos(OPERATORS[i], programLine);
         if position <> 0 then
         begin
            inc(result);
            delete(programLine, position, Length(OPERATORS[i]))
         end
         else
            break;
      end;
   //�������� ������� ���������� ���������
end;

function FindOperator(const programLine, PerlOperator: string) : integer;
var
   editLine: shortString;
   i : integer;
begin
   editLine := PerlOperator + ' ';
   i := 1;
   result := 0;
   while (i < Length(programLine) + 1) do
   begin
      if pos(editLine, programLine, i) <> 0 then
      begin
         inc(result);
         i := pos(editLine, programLine, i) + 3;
      end
      else
         i := Length(programLine) + 1
   end;
end;

end.
